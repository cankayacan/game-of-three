# game-of-three-server

``` bash
cd server
npm i
npm start
```

# game-of-three-client

``` bash
cd client
npm i
npm start
```

Then navigate to http://localhost:8090
