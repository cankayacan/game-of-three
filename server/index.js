const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const Room = require('./room');

const socketsQueue = new Set();
const gameRooms = new Map();

let gameId = 0;

function setRoomSocketMapping(room, firstClient, secondClient) {
  gameRooms.set(firstClient.id, room);
  gameRooms.set(secondClient.id, room);
  console.log('created room', room.id);
}

function removeRoomSocketMapping(room) {
  gameRooms.delete(room.firstClient.id);
  gameRooms.delete(room.secondClient.id);
  console.log('deleted room', room.id);
}

function createRoomWithLastTwoSockets() {
  const socketsArray = Array.from(socketsQueue);
  if (socketsArray.length >= 2) {
    // get the two sockets from the queue & join the room
    const firstClient = socketsArray.shift();
    const secondClient = socketsArray.shift();
    const room = new Room(gameId, firstClient, secondClient);
    setRoomSocketMapping(room, firstClient, secondClient);
    room.gameFinished = () => removeRoomSocketMapping(room);
    // remove the sockets from the starting queue
    socketsQueue.delete(firstClient);
    socketsQueue.delete(secondClient);
    gameId += 1;
  }
}

io.on('connection', (socket) => {
  socket.on('disconnect', () => {
    socketsQueue.delete(socket);
    const myRoom = gameRooms.get(socket.id);
    if (myRoom) {
      myRoom.disconnect();
    }
  });
  socket.on('add', (amount) => {
    const myRoom = gameRooms.get(socket.id);
    // allow clients to play in the right order
    if (myRoom && myRoom.clientToPlay === socket) {
      myRoom.addAmount(amount);
    }
  });
  socket.on('startRequest', (username) => {
    socket.username = username;
    socketsQueue.add(socket);
    createRoomWithLastTwoSockets();
  });
});

app.use(express.static(__dirname + '/dist'));

app.get('/', function(req, res) {
  res.sendfile(__dirname + '/dist/index.html');
});

http.listen((process.env.PORT || 3000), () => {
  console.log('listening on *:3000');
});
