const maxNumber = Math.pow(2, 32) - 1;

class Room {
  constructor(roomId, firstClient, secondClient) {
    this.value = Math.round(Math.random() * maxNumber);

    this.id = roomId;
    this.firstClient = firstClient;
    this.secondClient = secondClient;
    this.clientToPlay = secondClient;

    firstClient.join(roomId);
    secondClient.join(roomId);

    this.sendStart();
    this.sendValue();
  }

  sendStart() {
    this.firstClient.emit('start', {
      leftSide: true,
      opponentUsername: this.secondClient.username
    });
    this.secondClient.emit('start', {
      leftSide: false,
      opponentUsername: this.firstClient.username
    });
  }

  sendValue() {
    this.clientToPlay = this.clientToPlay === this.firstClient ? this.secondClient : this.firstClient;
    this.firstClient.emit('newNumber', {value: this.value, isMyTurn: this.clientToPlay === this.firstClient});
    this.secondClient.emit('newNumber', {value: this.value, isMyTurn: this.clientToPlay !== this.firstClient});
    console.log('send value', this.value);
  }

  addAmount(amount) {
    if ((this.value + amount) % 3 === 0) {
      this.value = (this.value + amount) / 3;
      if (this.value === 1) {
        this.sendResult(this.clientToPlay === this.firstClient);
      }
      this.sendValue();
    } else { // wrong input from the user
      this.sendResult(this.clientToPlay !== this.firstClient);
    }
  }

  disconnect() {
    this.firstClient.leave(this.id);
    this.secondClient.leave(this.id);

    this.firstClient.emit('opponentLeft');
    this.secondClient.emit('opponentLeft');

    this.gameFinished();
  }

  sendResult(isFirstClientTheWinner) {
    this.firstClient.emit('result', isFirstClientTheWinner);
    this.secondClient.emit('result', !isFirstClientTheWinner);
    this.disconnect();
    console.log('result', isFirstClientTheWinner);
  }
}

module.exports = Room;
