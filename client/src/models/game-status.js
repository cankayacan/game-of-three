export default {
  init: 'init',
  startRequested: 'startRequested',
  started: 'started',
  opponentDisconnected: 'opponentDisconnected',
  won: 'won',
  lost: 'lost'
};
