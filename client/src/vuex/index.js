import Vue from 'vue';
import Vuex from 'vuex';

import GameStatus from '../models/game-status';

Vue.use(Vuex);

const state = {
  gameStatus: GameStatus.init,
  value: 0,
  leftSide: false,
  isMyTurn: false,
  opponentUsername: ''
};

const mutations = {
  UPDATE_GAME_STATUS(s, gameStatus) {
    state.gameStatus = gameStatus;
  },
  SOCKET_START(s, startPayload) {
    state.gameStatus = GameStatus.started;
    state.leftSide = startPayload.leftSide;
    state.opponentUsername = startPayload.opponentUsername;
  },
  SOCKET_NEWNUMBER(s, newNumberPayload) {
    state.value = newNumberPayload.value;
    state.isMyTurn = newNumberPayload.isMyTurn;
  },
  SOCKET_RESULT(s, isWinner) {
    state.gameStatus = isWinner ? GameStatus.won : GameStatus.lost;
  },
  SOCKET_OPPONENTLEFT() {
    if (state.gameStatus === GameStatus.started) {
      state.gameStatus = GameStatus.opponentDisconnected;
    }
  }
};

const actions = {
  startRequest({ commit }) {
    commit('UPDATE_GAME_STATUS', GameStatus.startRequested);
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions
});
