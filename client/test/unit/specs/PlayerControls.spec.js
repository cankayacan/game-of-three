/* eslint-disable no-unused-expressions, no-undef */

import Vue from 'vue';
import PlayerControls from '@/components/PlayerControls';
import GameStatus from '@/models/game-status';

describe('PlayerControls.vue', () => {
  const createVm = (isMyTurn) => {
    const Constructor = Vue.extend(PlayerControls);
    const vm = new Constructor({
      propsData: { isMyTurn }
    }).$mount();
    return vm;
  };

  it('should disable add button when it is not my turn', () => {
    const vm = createVm(false);
    expect(vm.$el.querySelector('.controls .btn')).to.have.attr('disabled');
  });

  it('should enable add button when it is my turn', () => {
    const vm = createVm(true);
    expect(vm.$el.querySelector('.controls .btn')).to.not.have.attr('disabled');
  });

  describe('addAmount', () => {
    it('should emit addAmount with the specified amount', (done) => {
      const vm = createVm(GameStatus.startRequested);
      const spyOnEmit = chai.spy.on(vm, '$emit');

      vm.addAmount(-1);

      Vue.nextTick(() => {
        expect(spyOnEmit).to.have.been.called.with('addAmount', -1);
        done();
      });
    });
  });
});
