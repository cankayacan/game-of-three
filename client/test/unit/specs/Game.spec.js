/* eslint-disable no-unused-expressions, no-undef */

import Vue from 'vue';
import Game from '@/components/Game';
import GameStatus from '@/models/game-status';
import Vuex from 'vuex';

describe('Game.vue', () => {
  let testUsername = '';
  const initalState = {
    gameStatus: GameStatus.init,
    value: 0,
    leftSide: false,
    isMyTurn: false,
    opponentUsername: ''
  };

  beforeEach(() => {
    testUsername = Math.random().toString(36).substring(7);
  });

  const createVm = (testState) => {
    const Constructor = Vue.extend(Game);
    return new Constructor({
      store: new Vuex.Store({
        state: { ...initalState, ...testState }
      })
    }).$mount();
  };

  it('should correctly get the state', () => {
    const state = {
      gameStatus: GameStatus.started,
      value: 123,
      leftSide: false,
      isMyTurn: true,
      opponentUsername: testUsername
    };

    const vm = createVm(state);

    expect(vm.gameStatus).to.equal(state.gameStatus);
    expect(vm.value).to.equal(state.value);
    expect(vm.leftSide).to.equal(state.leftSide);
    expect(vm.isMyTurn).to.equal(state.isMyTurn);
    expect(vm.opponentUsername).to.equal(state.opponentUsername);
  });

  it('should display game-starter when the game has not yet started', (done) => {
    const vm = createVm({ gameStatus: GameStatus.init });
    Vue.nextTick(() => {
      expect(vm.$el.querySelectorAll('game-starter')).to.not.be.null;
      done();
    });
  });

  it('should display board and player-controls when the game has started', (done) => {
    const vm = createVm({ gameStatus: GameStatus.started });
    Vue.nextTick(() => {
      expect(vm.$el.querySelectorAll('board')).to.not.be.null;
      expect(vm.$el.querySelectorAll('player-controls')).to.not.be.null;
      done();
    });
  });

  describe('addAmount', () => {
    it('should emit add with the specified amount to the socket', (done) => {
      const socket = chai.spy.object(['emit']);
      Vue.prototype.$socket = socket;
      const vm = createVm({ gameStatus: GameStatus.started });

      vm.addAmount(-1);

      Vue.nextTick(() => {
        expect(socket.emit).to.have.been.called.with('add', -1);
        done();
      });
    });
  });

  describe('startGame', () => {
    it('should call socket.emit with "startRequest" param and the username', (done) => {
      const socket = chai.spy.object(['emit']);
      Vue.prototype.$socket = socket;
      const vm = createVm({ gameStatus: GameStatus.started });

      vm.startGame({ myUsername: testUsername, autoPlay: true });

      Vue.nextTick(() => {
        expect(vm.autoPlay).to.be.true;
        expect(socket.emit).to.have.been.called.with('startRequest', testUsername);
        done();
      });
    });
  });

  describe('playAutomatically', () => {
    it('should call socket.emit with "add" param and the amount', (done) => {
      const socket = chai.spy.object(['emit']);
      Vue.prototype.$socket = socket;
      const vm = createVm({ gameStatus: GameStatus.started });

      // ignore the timeout in unit tests
      vm.setTimeout = (fn) => fn();

      vm.playAutomatically();

      Vue.nextTick(() => {
        expect(socket.emit).to.have.been.called.with('add');
        done();
      });
    });
  });
});
