import Vue from 'vue';
import GameResult from '@/components/GameResult';
import GameStatus from '@/models/game-status';

describe('GameResult.vue', () => {
  const createVm = (gameStatus) => {
    const Constructor = Vue.extend(GameResult);
    const vm = new Constructor({
      propsData: { gameStatus }
    }).$mount();
    return vm;
  };

  it('should render correct content when gameStatus is GameStatus.won', () => {
    const vm = createVm(GameStatus.won);
    expect(vm.$el.querySelector('.game-result > h1').textContent).to.equal('You won!');
  });

  it('should render correct content when gameStatus is GameStatus.lost', () => {
    const vm = createVm(GameStatus.lost);
    expect(vm.$el.querySelector('.game-result > h1').textContent).to.equal('You lost!');
  });

  it('should render correct content when gameStatus is GameStatus.startRequested', () => {
    const vm = createVm(GameStatus.startRequested);
    expect(vm.$el.querySelector('.game-result > h1').textContent).to.equal('Waiting for an opponent!');
  });
});
