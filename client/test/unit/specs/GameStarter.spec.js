/* eslint-disable no-unused-expressions, no-undef */

import Vue from 'vue';
import GameStarter from '@/components/GameStarter';
import GameStatus from '@/models/game-status';

describe('GameStarter.vue', () => {
  const createVm = (gameStatus) => {
    const Constructor = Vue.extend(GameStarter);
    const vm = new Constructor({
      propsData: { gameStatus }
    }).$mount();
    return vm;
  };

  it('should render username input when gameStatus is GameStatus.init', () => {
    const vm = createVm(GameStatus.init);
    expect(vm.$el.querySelector('.game-start input')).to.not.be.null;
  });

  it('should not render username input when gameStatus is not GameStatus.init', () => {
    const vm = createVm(GameStatus.started);
    expect(vm.$el.querySelector('.game-start input')).to.be.null;
  });

  it('should disable start button when gameStatus is GameStatus.startRequested', () => {
    const vm = createVm(GameStatus.startRequested);
    expect(vm.$el.querySelector('.game-start .btn')).to.have.attr('disabled');
  });

  it('should enable start button when gameStatus is GameStatus.init', () => {
    const vm = createVm(GameStatus.init);
    expect(vm.$el.querySelector('.game-start .btn')).to.not.have.attr('disabled');
  });

  describe('startGame', () => {
    it('should emit startGame with the given username', (done) => {
      const vm = createVm(GameStatus.startRequested);
      const spyOnEmit = chai.spy.on(vm, '$emit');
      const testUsername = Math.random().toString(36).substring(7);
      vm.username = testUsername;

      vm.startGame(false);

      Vue.nextTick(() => {
        expect(spyOnEmit).to.have.been.called.with('startGame', { myUsername: testUsername, autoPlay: false });
        done();
      });
    });

    it('should emit startGame withe the specified autoplay parameter', (done) => {
      const vm = createVm(GameStatus.startRequested);
      const spyOnEmit = chai.spy.on(vm, '$emit');
      vm.username = '';

      vm.startGame(true);

      Vue.nextTick(() => {
        expect(spyOnEmit).to.have.been.called.with('startGame', { myUsername: '', autoPlay: true });
        done();
      });
    });
  });
});
